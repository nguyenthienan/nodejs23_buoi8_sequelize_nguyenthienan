const sequelize = require("../models/index");
const initModel = require("../models/init-models");
const model = initModel(sequelize);
const { successCode, errorCode, failCode } = require("../ulti/response");

// 1. xu ly like, unlike, lay ds like theo restaurant va user
const createLikeRestaurant = async (req, res) => {
  const { user_id, res_id, date_like = null } = req.body;

  let object = {
    user_id,
    res_id,
    date_like,
  };

  let checkLike = await model.like_res.findAll({
    where: { user_id: user_id, res_id: res_id },
  });
  if (checkLike.length == 0) {
    await model.like_res.create(object);
    res.status(200).send("Đã like!!");
  } else {
    await model.like_res.destroy({
      where: { user_id: user_id, res_id: res_id },
    });
    res.status(200).send("Đã unlike!!");
  }
};

const getLikeByRes = async (req, res) => {
  const data = await model.like_res.findAll({ include: ["re", "user"] });

  successCode(res, data);
};

const getLikeByUser = async (req, res) => {
  const data = await model.like_res.findAll({ include: ["user", "re"] });

  successCode(res, data);
};

// 2. Xử lý đánh giá nhà hàng (thêm đánh giá, lấy danh sách đánh theo nhà hàng và user)

const createRateRestaurant = async (req, res) => {
  const { user_id, res_id, amount, date_rate = null } = req.body;

  let object = {
    user_id,
    res_id,
    amount,
    date_rate,
  };

  let checkRate = await model.rate_res.findAll({
    where: { user_id: user_id, res_id: res_id },
  });
  if (checkRate.length == 0) {
    let data = await model.rate_res.create(object);
    res.status(200).send(data);
  } else {
    let data = await model.rate_res.update(object, {
      where: { user_id: user_id, res_id: res_id },
    });
    res.status(200).send("Đã update Rate");
  }
};

const getRateByRes = async (req, res) => {
  const data = await model.rate_res.findAll({ include: ["re", "user"] });

  successCode(res, data);
};

const getRateByUser = async (req, res) => {
  const data = await model.rate_res.findAll({ include: ["user", "re"] });

  successCode(res, data);
};

// 3. User đặt món (thêm order)

const createOrder = async (req, res) => {
  const { user_id, food_id, amount, code = null, arr_sub_id = null } = req.body;

  let object = {
    user_id,
    food_id,
    amount,
    code,
    arr_sub_id,
  };

  let checkRate = await model.order.findAll({
    where: { user_id: user_id, food_id: food_id },
  });
  if (checkRate.length == 0) {
    await model.order.create(object);
    res.status(200).send("Đã order");
  } else {
    await model.order.update(object, {
      where: { user_id: user_id, food_id: food_id },
    });
    res.status(200).send("Đã update order");
  }
};

module.exports = {
  createLikeRestaurant,
  getLikeByRes,
  getLikeByUser,
  createRateRestaurant,
  getRateByRes,
  getRateByUser,
  createOrder,
};
