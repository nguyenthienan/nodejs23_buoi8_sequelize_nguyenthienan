const express = require("express");
const userController = require("../../controllers/userController");

const userRouter = express.Router();

userRouter.get("/getUser", userController.getUser);
userRouter.post("/createUser", userController.createUser);

module.exports = userRouter;
