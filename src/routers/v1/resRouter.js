const express = require("express");
const resController = require("../../controllers/resController");

const resRouter = express.Router();

// 1. xu ly like, unlike, lay ds like theo restaurant va user

// POST like restaurant
resRouter.post("/createLikeRes", resController.createLikeRestaurant);
// GEt like by restaurant
resRouter.get("/getLikeByRes", resController.getLikeByRes);
// GET like by user
resRouter.get("/getLikeByUser", resController.getLikeByUser);

// 2. Xử lý đánh giá nhà hàng (thêm đánh giá, lấy danh sách đánh theo nhà hàng và user)

// POST Create rate res
resRouter.post("/createRateRes", resController.createRateRestaurant);
// GEt like by restaurant
resRouter.get("/getRateByRes", resController.getRateByRes);
// GET like by user
resRouter.get("/getRateByUser", resController.getRateByUser);

// 3. User đặt món (thêm order)
// POST Create Order
resRouter.post("/createOrder", resController.createOrder);

module.exports = resRouter;
