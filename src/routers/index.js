const express = require('express');
const rootRouter = express.Router();

const userRouter = require('./v1/userRouter');
const resRouter = require('./v1/resRouter');

rootRouter.use("/user/v1",userRouter);
rootRouter.use('/res/v1',resRouter);

module.exports=rootRouter;