const express = require("express");
const mysql = require("mysql2");

const app = express();
app.use(express.json());

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With,Content-Type,Accept"
  );
  next();
});

const conn = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "1234",
  database: "DB_FOOD",
  port: 3306,
});

app.listen(8080);

const rootRouter = require("./routers/index");

app.use("/api", rootRouter);
