const successCode = (res, data) => {
  let dSend = {
    message: "success",
    data: data,
  };
  res.status(200).send(dSend);
};

const errorCode = (res, data) => {
  let dSend = {
    message: "error",
    data: data,
  };
  res.status(400).send(dSend);
};

const failCode = (res) => {
  let dSend = {
    message: "error server",
  };
  res.status(500).send(dSend);
};

module.exports = { successCode, errorCode, failCode };
