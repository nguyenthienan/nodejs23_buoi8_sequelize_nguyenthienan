const { Sequelize } = require("sequelize");
const config = require("../config/index");

const sequelize = new Sequelize(
  config.db_database,
  config.db_user,
  config.db_password,
  {
    host: config.db_host,
    dialect: config.db_dialect,
    port: config.db_port,
  }
);

module.exports = sequelize;

// generate all table from database DB_FOOD
// yarn sequelize-auto -h localhost -d DB_FOOD -u root -x 1234 -p 3306 --dialect mysql -o src/models -l es6
